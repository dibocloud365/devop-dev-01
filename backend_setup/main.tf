provider "aws" {
    region = "us-east-1"
}

resource "aws_s3_bucket" "devops-bucket" {
  bucket = "dibo-dev-tfstate"
  /* acl = "private" */
  tags = {
    Name        = "Dibo-bucket"
    Environment = "Dev"
  }
}

resource "aws_dynamodb_table" "dibo-terraform-state-lock-dynamodb-table" {
  name           = "dibo-terraform-state-lock"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

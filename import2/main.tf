provider "aws" {
    region = "us-east-1"
}


resource "aws_vpc" "vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name = "Original VPC"
    }
}

resource "aws_vpc" "vpc2" {
    cidr_block = "172.16.0.0/16"
    tags = {
        Name = "Imported VPC"
    }
}
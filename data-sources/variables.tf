variable "region" {
    type = string
    default = "us-east-1"
}

variable "ami" {
    type = string
    default = "ami-0742b4e673072066f"
}

variable "type" {
    type = string 
    default = "t2.micro"
}

variable "tags" {
    type = map 
    default = {
        Name = "WebServer"
        Env = "Dev"
    }
}

variable "bucketlist" {
    type = list(string)
    default = ["dibo-100-buc","dibo-200-buc","dibo-300-buc"] 
}

variable "sshport" {
    type = number 
    default = 22
}

/*variable "inputname" {
  type        = string
  description = "Set the name of the VPC"
}
*/

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "tcp" {
    type = string
    default = "tcp"
}

variable "ssh" {
    type = string
    default = "tcp"
}

variable "httpsport" {
    type = number 
    default = 443
}

variable "count-ec2" {
    type = number
    default = 5
}

variable "subnet-cidr-public" {
    type = string
    default = "10.0.2.0/24"
}